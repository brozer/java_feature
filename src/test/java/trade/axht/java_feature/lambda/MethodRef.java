package trade.axht.java_feature.lambda;

import org.junit.Test;

import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.function.*;

/**
 * 描述:
 * Lambda方法引用
 *
 * @outhor Administrator
 * @create 2017-11-15 22:44
 */
public class MethodRef {

    @Test
    public void test1(){
        PrintStream ps=System.out;
        Consumer<String> consumer=(x)-> System.out.println(x);
        consumer.accept("Love forever");

        System.out.println("---------------------");

        Consumer<String> consumer1=System.out::println;
        consumer1.accept("Until Dies");

    }

    //对象的引用::实例方法名
    @Test
    public void test2(){
        Employee emp=new Employee("101","张三",18,BigDecimal.valueOf(21532.42523));
        Supplier supplier=()->emp.getName();
        System.out.println(supplier.get());
        System.out.println("----------------------");
        Supplier<String> supplier1=emp::getName;
        System.out.println(supplier1.get());
    }

    @Test
    public void test3(){
        BiFunction<Double,Double,Double> function=(x,y)->Math.max(x,y);
        Double theMax=function.apply(100d,2d);
        System.out.println(theMax);
        System.out.println("------------------------");
        BiFunction<Double,Double,Double> function1=Math::max;
        System.out.println(function1.apply(11d,22d));

    }

    //类名：静态方法名
    @Test
    public void test4(){
        BiFunction<Integer,Integer,Integer> com=(x, y)->Integer.compare(x,y);
        System.out.println(com.apply(1,3));
        System.out.println("--------------------");
        BiFunction<Integer,Integer,Integer> function=Integer::compare;
        System.out.println(function.apply(3,1));
    }

    @Test
    public void test5(){
        Comparator<Integer> comparator=Integer::compare;
        System.out.println("---------------");
        System.out.println(comparator.compare(1,2));
    }

    @Test
    public void test6(){
        Employee employee=new Employee();
        Predicate<Employee> predicate=e -> e.getName()==null;
        System.out.println(predicate.test(employee));

        BiPredicate<String,String>  bp=(x,y)->x.equals(y);
        System.out.println(bp.test("abcde","abcde"));
        System.out.println("-----------------------------");
        BiPredicate<String,String> biPredicate=String::equals;
        System.out.println(biPredicate.test("abcde","abcde"));

    }

    //构造器引用
    @Test
    public void test7(){
        Function<String,Employee> function=(str)->new Employee(str);
        Employee employee=function.apply("张无忌");
        System.out.println(employee.getName());
        System.out.println("-------------------------");
        Function<String,Employee> function1=Employee::new;
        Employee employee1=function1.apply("张三丰");
        System.out.println(employee1.getName());
    }








}
