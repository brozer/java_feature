package trade.axht.java_feature.lambda;

import java.math.BigDecimal;

/**
 * 描述: 员工信息
 *
 * @outhor Administrator
 * @create 2017-11-15 23:08
 */
public class Employee {
    private String id;
    private String name;
    private Integer age;
    private BigDecimal money;

    public Employee(String id, String name, Integer age, BigDecimal money) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.money = money;
    }

    public Employee(){}

    public Employee(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }


}
