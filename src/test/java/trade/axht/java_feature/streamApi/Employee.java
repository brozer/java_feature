package trade.axht.java_feature.streamApi;

import java.math.BigDecimal;

/**
 * 描述: 员工信息
 *
 * @outhor Administrator
 * @create 2017-11-15 23:08
 */
public class Employee {
    private Integer id;
    private String name;
    private Integer age;
    private Double money;
    private Status status;

    public Employee(Integer id, String name, Integer age, Double money) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.money = money;
    }

    public Employee(Integer id, String name, Integer age, Double money, Status status) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.money = money;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    enum Status{
        Free,
        Busy,
        Normal
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", money=" + money +
                ", status=" + status +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employee employee = (Employee) o;

        if (id != null ? !id.equals(employee.id) : employee.id != null) return false;
        if (name != null ? !name.equals(employee.name) : employee.name != null) return false;
        if (age != null ? !age.equals(employee.age) : employee.age != null) return false;
        return money != null ? money.equals(employee.money) : employee.money == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (age != null ? age.hashCode() : 0);
        result = 31 * result + (money != null ? money.hashCode() : 0);
        return result;
    }
}
