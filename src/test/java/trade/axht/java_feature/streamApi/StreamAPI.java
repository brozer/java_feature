package trade.axht.java_feature.streamApi;

import org.junit.Test;

import javax.swing.text.html.Option;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 描述:
 * StreamAPI 的操作
 *
 * @outhor Administrator
 * @create 2017-11-19 16:25
 */
public class StreamAPI {

    //创建Stream
    @Test
    public void test1(){
        //1 Collection 提供了两个方法 stream()与parallelStream()
        List<String> list=new ArrayList<>();
        Stream<String> stream=list.stream();//获取一个顺序流
        Stream<String> parallelStream=list.parallelStream();//获取一个并行流

        Integer[] nums=new Integer[10];
        Stream<Integer> stream1= Arrays.stream(nums);

        //3 通过Stream类中的静态方法of（）
        Stream<Integer> intStream=Stream.of(1,2,3,4,5);

        //4 创建无限流
        Stream<Integer> streamLoo=Stream.iterate(0,(x)->x+2).limit(10);
        streamLoo.forEach(System.out::println);

    }


    //2. 中间操作
    List<Employee> emps = Arrays.asList(
            new Employee(102, "李四", 59, 6666.66, Employee.Status.Busy),
            new Employee(101, "张三", 18, 9999.99,Employee.Status.Free),
            new Employee(103, "王五", 28, 3333.33,Employee.Status.Busy),
            new Employee(104, "赵六", 8, 7777.77,Employee.Status.Free),
            new Employee(104, "赵六", 8, 7777.77,Employee.Status.Busy),
            new Employee(104, "赵六", 8, 7777.77,Employee.Status.Free),
            new Employee(105, "田七", 38, 5555.55,Employee.Status.Busy)
    );

    /*
	  筛选与切片
		filter——接收 Lambda ， 从流中排除某些元素。
		limit——截断流，使其元素不超过给定数量。
		skip(n) —— 跳过元素，返回一个扔掉了前 n 个元素的流。若流中元素不足 n 个，则返回一个空流。与 limit(n) 互补
		distinct——筛选，通过流所生成元素的 hashCode() 和 equals() 去除重复元素
	 */
    //内部迭代：迭代操作 Stream API 内部完成
    @Test
    public void test2(){
        //所有的中间操作不会做任何处理
        Stream<Employee> stream=emps.stream().filter(e->{
            System.out.println("测试中间操作");
            return e.getAge()<=35;
        });
        //只有当做终止操作时，所有的中间操作会一次性的全部执行，称为“惰性求值”
        stream.forEach(System.out::println);
    }

    @Test
    public void test3(){
        emps.stream().filter(e->{
            System.out.println("短路");
            return e.getMoney()>=5000;
        }).limit(3).forEach(System.out::println);
    }

    @Test
    public void test4(){
        emps.stream().filter(e->{
            System.out.println("短路");
            return e.getMoney()>=5000;
        }).skip(2).forEach(System.out::println);
    }

    @Test
    public void test5(){
        emps.stream().distinct().forEach(System.out::println);
    }


    //2. 中间操作
	/*
		映射
		map——接收 Lambda ， 将元素转换成其他形式或提取信息。接收一个函数作为参数，该函数会被应用到每个元素上，并将其映射成一个新的元素。
		flatMap——接收一个函数作为参数，将流中的每个值都换成另一个流，然后把所有流连接成一个流
	 */

    @Test
    public void test6(){
        Stream<String> str=emps.stream().map(Employee::getName);
        //str.forEach(System.out::println);
        List<String> strList=Arrays.asList("aaa","bbb","ccc","ddd","eee");
        Stream<String> stream=strList.stream().map(String::toUpperCase);
        stream.forEach(System.out::println);
        System.out.println("----------------");
        Stream<Stream<Character>> stream2=strList.stream().map(StreamAPI::filterCharacter);
        stream2.forEach(sm->sm.forEach(System.out::print));
        System.out.println("-------------------");
        Stream<Character> stream3=strList.stream().flatMap(StreamAPI::filterCharacter);
        stream3.forEach(System.out::println);

    }

    public static Stream<Character> filterCharacter(String str){
        List<Character> list=new ArrayList<>();
        for (Character ch:str.toCharArray()){
            list.add(ch);
        }
        return list.stream();

    }

    /*
        sorted()-----自然排序
        sorted(Comparator com) ------定制排序
     */
    @Test
    public void test7(){
        emps.stream().map(Employee::getName).sorted().forEach(System.out::println);
        System.out.println("------------------");
        emps.stream().sorted((x,y)->{
            if(x.getAge()==y.getAge()){
                return x.getName().compareTo(y.getName());
            }else {
                return -Integer.compare(x.getAge(),y.getAge());
            }
        }).forEach(System.out::println);
    }

    //3. 终止操作
	/*
		allMatch——检查是否匹配所有元素
		anyMatch——检查是否至少匹配一个元素
		noneMatch——检查是否没有匹配的元素
		findFirst——返回第一个元素

		findAny——返回当前流中的任意元素
		count——返回流中元素的总个数
		max——返回流中最大值
		min——返回流中最小值
	 */

	@Test
	public void test8(){
	    boolean b1=emps.stream().allMatch(e->e.getStatus().equals(Employee.Status.Busy));
        System.out.println(b1);

        boolean b2=emps.stream().anyMatch(e->e.getStatus().equals(Employee.Status.Busy));
        System.out.println(b2);

        boolean b3=emps.stream().noneMatch(e->e.getStatus().equals(Employee.Status.Normal));
        System.out.println(b3);
    }

    @Test
    public void test9(){
        Optional<Employee> op=emps.stream().sorted((e1,e2)->-Double.compare(e1.getMoney(),e2.getMoney())).findFirst();
        System.out.println(op.get());

        Optional<Employee> op2=emps.parallelStream().filter(e->e.getStatus().equals(Employee.Status.Busy)).findAny();
        System.out.println(op2.get());
    }


    @Test
    public void test10(){
        long count=emps.stream().filter(e->e.getStatus().equals(Employee.Status.Busy)).count();
        System.out.println(count);

        Optional<Double> max=emps.stream().map(Employee::getMoney).max(Double::compare);
        System.out.println(max.get());

        Optional<Double> min=emps.stream().map(Employee::getMoney).min(Double::compare);
        System.out.println(min.get());
    }

    @Test
    public void test11(){
        Stream<Employee> stream=emps.stream().filter(e->e.getStatus().equals(Employee.Status.Free));
        long count=stream.count();
        System.out.println(count);
        stream.map(Employee::getMoney).max(Double::compare);//报错:流进行了终止操作后，不能再次使用
    }


    //3. 终止操作
	/*
		归约
		reduce(T identity, BinaryOperator) / reduce(BinaryOperator) ——可以将流中元素反复结合起来，得到一个值。
	 */

	@Test
	public void test12(){
	    List<Integer> list=Arrays.asList(1,2,3,4,5,6,7,8,9,10);
	    Integer sum=list.stream().reduce(0,(x,y)->x+y);
        System.out.println(sum);
    }

    @Test
    public void test13(){
	    List<Employee> employees=emps.stream().filter(e->e.getName().contains("六")).collect(Collectors.toList());
        System.out.println(employees);

        Integer sum=emps.stream().map(Employee::getName).flatMap(StreamAPI::filterCharacter).map(ch->{
            if(ch.equals('六'))
                return 1;
            else
                return 0;
        }).reduce(0,Integer::sum);
        System.out.println(sum);
    }

    //collect——将流转换为其他形式。接收一个 Collector接口的实现，用于给Stream中元素做汇总的方法
    @Test
    public void test14(){
        List<String> list=emps.stream().map(Employee::getName).collect(Collectors.toList());
        list.forEach(System.out::println);
        System.out.println("=================");
        Set<String> hs=emps.stream().map(Employee::getName).collect(Collectors.toSet());
        hs.forEach(System.out::println);
        System.out.println("==============");
        HashSet<String> hashSet=emps.stream().map(Employee::getName).collect(Collectors.toCollection(()->{return new HashSet<>();}));
        hashSet.forEach(System.out::println);
    }

    @Test
    public void test15(){
        Optional<Double> max=emps.stream().map(Employee::getMoney).collect(Collectors.maxBy(Double::compare));
        System.out.println(max.get());

        Optional<Double> min=emps.stream().map(Employee::getMoney).collect(Collectors.minBy(Double::compare));
        System.out.println(min.get());

        Double sum=emps.stream().map(Employee::getMoney).collect(Collectors.summingDouble(e->e));
        System.out.println(sum);

        Double sum2=emps.stream().collect(Collectors.summingDouble(Employee::getMoney));
        System.out.println(sum2);

        Double average=emps.stream().collect(Collectors.averagingDouble(Employee::getMoney));
        System.out.println(average);

        Long count=emps.stream().collect(Collectors.counting());
        System.out.println(count);

        DoubleSummaryStatistics dss=emps.stream().collect(Collectors.summarizingDouble(Employee::getMoney));
        System.out.println(dss.getMax());

    }
    //分组
    @Test
    public void test16(){
        Map<Employee.Status,List<Employee>> map=emps.stream().collect(Collectors.groupingBy(Employee::getStatus));
        System.out.println(map);
        for(Map.Entry<Employee.Status,List<Employee>> entry:map.entrySet()){
            System.out.println(entry.getKey()+":"+entry.getValue());
        }
    }

    @Test
    public void test17(){
        Map<Employee.Status,Map<String,List<Employee>>> map=emps.stream().collect(Collectors.groupingBy(Employee::getStatus,Collectors.groupingBy(e->{
         if(e.getAge()>=60)
             return "老年";
         else if (e.getAge()>=35)
             return "中年";
         else
             return "成年";
        })));
        System.out.println(map);
    }
    //分区
    @Test
    public  void test18(){
        Map<Boolean,List<Employee>> map=emps.stream().collect(Collectors.partitioningBy(e->e.getMoney()>=5000));
        System.out.println(map);
    }

    @Test
    public void test19(){
        String str=emps.stream().map(Employee::getName).collect(Collectors.joining("/","start","end"));
        System.out.println(str);
    }

    @Test
    public void test20(){
        Optional<Double> sum=emps.stream().map(Employee::getMoney).collect(Collectors.reducing(Double::sum));
        System.out.println(sum.get());
    }


}
